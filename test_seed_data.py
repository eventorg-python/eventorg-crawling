from models.source import Source
from faker import Faker

faker = Faker()

source = dict()
source["name"] = 'hyderabad-events'
source["url"] = 'https://www.hyderabadevents.com/'
source["code"] = 'HYD-EVENTS'

s_obj = Source.objects(code='HYD-EVENTS')
if not (s_obj):
    Source.create_source(source)

source = dict()
source["name"] = 'seneca-global-events'
source["url"] = 'https://www.senecaglobal.com/'
source["code"] = 'SENECA-EVENTS'

s_obj = Source.objects(code=source["code"])
if not (s_obj):
    Source.create_source(source)

source = dict()
source["name"] = 'seneca-global--hyderabad-events'
source["url"] = 'https://www.senecaglobalhyd.com/'
source["code"] = 'SENECA-EVENTS-HYD'

s_obj = Source.objects(code=source["code"])
if not (s_obj):
    Source.create_source(source)

source = dict()
source["name"] = 'google-hyderabad-events'
source["url"] = faker.url()
source["code"] = 'SENECA-EVENTS-USA-L'

s_obj = Source.objects(code=source["code"])
if not (s_obj):
    Source.create_source(source)

source = dict()
source["name"] = "a-events"
source["url"] = faker.url()
source["code"] = 'SENECA-EVENTS-USA'

s_obj = Source.objects(code=source["code"])
if not (s_obj):
    Source.create_source(source)

source = dict()
source["name"] = "b-events"
source["url"] = faker.url()
source["code"] = 'SENECA-EVENTS-UK'

s_obj = Source.objects(code=source["code"])
if not (s_obj):
    Source.create_source(source)

source = dict()
source["name"] = "c-events"
source["url"] = faker.url()
source["code"] = 'SENECA-EVENTS-JERMAN'

s_obj = Source.objects(code=source["code"])
if not (s_obj):
    Source.create_source(source)

source = dict()
source["name"] = "d-events"
source["url"] = faker.url()
source["code"] = 'TEST-ONE-THREE'

s_obj = Source.objects(code=source["code"])
if not (s_obj):
    Source.create_source(source)

source = dict()
source["name"] = "e-events"
source["url"] = faker.url()
source["code"] = 'TEST-ONE-TWO'

s_obj = Source.objects(code=source["code"])
if not (s_obj):
    Source.create_source(source)

source = dict()
source["name"] = "f-events"
source["url"] = faker.url()
source["code"] = 'TEST-ONE-CODE'

s_obj = Source.objects(code=source["code"])
if not (s_obj):
    Source.create_source(source)
