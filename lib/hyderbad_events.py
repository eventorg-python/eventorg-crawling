try:
    import urllib2
except ImportError:
    import urllib.request as urllib2

from bs4 import BeautifulSoup
import json
import datetime


class HyderabadEvents:

    def download_page(self, url):
        req = urllib2.Request(url)
        page = urllib2.urlopen(req)
        soup = BeautifulSoup(page, "html.parser")
        return soup

    def doc3(self, url):
        url = url
        resp = self.download_page(url)
        try:
            image = resp.find('img', attrs={'alt': 'Banner Image'})['src']
        except:
            image = " "

        info_box = resp.find('div', attrs={'style': 'word-wrap: break-word; text-align: justify;'}).find_all('p')
        details = []
        for info in info_box:
            details.append(info.text.strip())
            about = " ".join(details)
            title = resp.find('h1', attrs={'class': 'page-title'}).text.strip()
            contact_info = {}
            contact_box = resp.find_all('div', attrs={'class': 'col-md-6'})
            data = contact_box[2].find_all('div', attrs={'class': 'row'})
            for i in range(1, 8):
                attr_name = data[i].find('div', attrs={'class': 'col-md-4'}).text.replace(':', '').strip()
                attr_value = data[i].find('div', attrs={'class': 'col-md-8'}).text.strip()
                contact_info[attr_name] = attr_value
            result = {}
            result['title'] = title
            result['event_logo'] = image
            result['about'] = about
            result['contact_info'] = contact_info
            return result

    def doc2(self, url):
        url = url
        resp = self.download_page(url)
        name_box = resp.find('div', attrs={'class': 'success-story'})
        if name_box is None:
            pass
        else:
            places = name_box.find_all('div', attrs={'class': 'col-sm-3'})
            result2 = {}
            for place in places:
                print('***')
                print(place.find('a')['href'])
                result = self.doc3(place.find('a')['href'])
                result2[place.find('a')['href'].split("/")[-1]] = result
            return result2

    def doc1(self, url):
        resp = self.download_page(url)
        events_div = resp.find('div', attrs={'class': 'service-catagari'})
        events = events_div.find_all('li')
        final_result = {}
        for event in events:
            print(event.find('a')['href'])
            result3 = self.doc2(url + event.find('a')['href'])
            final_result[event.find('a')['href'].split("/")[-1]] = result3
        return final_result

    def start_crawling(self, url):
        final = self.doc1(url)
        name = datetime.datetime.now().strftime("%Y-%m-%d%H:%M:%S")
        document = name + ".json"
        with open(document, "w+") as outfile:
            json.dump(final, outfile, indent=4)
        return document
