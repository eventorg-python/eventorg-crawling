from lib import hyderbad_events
from models.event import Event
from models.source import Source


class Crawl:
    def __init__(self):
        sources = Source.objects()
        self.map_source(sources)

    def map_source(self, sources):
        for source in sources:
            if source['code'] == 'HYD-EVENTS':
                url = source['url']
                obj = hyderbad_events.HyderabadEvents()
                document = obj.start_crawling(url)
                Event.create_events(source, document)

Crawl()
