from datetime import datetime
from elasticsearch5 import Elasticsearch
es = Elasticsearch()

# doc = {
#     'author': 'kimchy',
#     'text': 'Elasticsearch: cool. bonsai cool.',
#     'timestamp': datetime.now(),
# }
#
for i in range(1, 100):
    doc = {
        'author': 'author ' + str(i),
        'text': 'Elastic Search: author ' + str(i),
        'timestamp': datetime.now(),
    }
    res = es.index(index="test-index", doc_type='tweet', id=i, body=doc)
print(res['result'])

# res = es.get(index="test-index", doc_type='tweet', id=1)
# print(res['_source'])

# es.indices.refresh(index="test-index")
#
# res = es.search(index="test-index", body={"query": {"match": {"author": "author"}}})
# print("Got %d Hits:" % res['hits']['total'])
# for hit in res['hits']['hits']:
#     print("%(timestamp)s %(author)s: %(text)s" % hit["_source"])