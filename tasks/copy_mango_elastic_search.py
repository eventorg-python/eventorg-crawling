from elasticsearch5 import Elasticsearch
from opt.settings.db_connection import DbConnection

es = Elasticsearch()


class BD:
    def __init__(self):

        self.db = DbConnection.connect_data_base_and_return(self)
        self.move_data_to_elastic_search()

    def move_data_to_elastic_search(self):
        # collection_name = self.db.collection_names(include_system_collections=True)
        event_info = {}
        events = self.db.event.find({})
        i = 1
        for event in events:
            event_info['title'] = event['title']
            event_info['updated_at'] = event['updated_at']
            event_info['created_at'] = event['created_at']
            event_info['event_info'] = event['event_info']
            event_info['contact_info'] = event['contact_info']
            event_info['event_tag_name'] = event['event_tag_name']
            event_info['source_id'] = event['source_id']
            es.index(index="event-index", doc_type='event-doc', id=i, body=event_info)
            event_info.clear()
            i = i + 1


BD()
