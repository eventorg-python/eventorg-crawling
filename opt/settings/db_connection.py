import os
from os.path import join, dirname
from dotenv import load_dotenv
from mongoengine import *


class DbConnection:
    def __init__(self, env):
        dotenv_path = join(dirname(__file__), '../../.env_' + env)
        load_dotenv(dotenv_path)

    def connect_database(self):
        connect(os.getenv('DATABASE_NAME'), host=os.getenv('HOST'))
