import os
from dotenv import load_dotenv
from opt.settings import db_connection
load_dotenv()

env = os.getenv('ENVIRONMENT')
print(env)

db_connection_obj = db_connection.DbConnection(env)
db_connection_obj.connect_database()
