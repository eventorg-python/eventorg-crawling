Crawling Events
======
Crawling Events is a modern, crwling data helpdesk solution written in Python(3.2). The goal of Crawling Events application is to crawl different sites of information and store database without duplicates and provide search capabilites.

### Technologies!
To achieve end of result crawlin-events below technologies and plugins are used.

| Plugin | Refrence |
| ------ | ------ |
| Python | https://www.python.org/ |
| MangoDb | https://www.mongodb.com/ |
| Elastic Search| https://www.elastic.co/|
| Mongoengine | http://mongoengine.org/ |
| Pytest | https://docs.pytest.org/en/latest/ |
| Python-dotenv | https://github.com/theskumar/python-dotenv |
| Pytest-mongodb | https://pypi.org/project/pytest-mongodb/ |
| Kibana | https://www.elastic.co/products/kibana |

### Installation
Crawling Events requires [Python]( https://www.python.org/) v3+ to run.
Install virtual environment On macOS and Linux and create and activate:
- [Virtual Environment] - To provide separate environment for project!

```sh
$ python3 -m pip install --user virtualenv
$ python3 -m virtualenv env
$ source env/bin/activate
```
In the second command generally second argument(env) name of your project.
Install the dependencies and devDependencies and start the server.
- [Python3] - To provide separate environment for project!
```sh
$ sudo apt-get update
$ sudo apt-get -y upgrade
$ sudo apt-get install -y python3-pip
$ python3 -V
```
- [Elastic Search] - Follow the below url to configure elastic search!
    https://www.digitalocean.com/community/tutorials/how-to-install-and-configure-elasticsearch-on-ubuntu-16-04
- [Elastic search configure with kibana]: By using kibana to run elastic search queries and visulize the elastic-search database.Install kibana follow the below url (Linux).
https://www.elastic.co/guide/en/kibana/current/deb.html
configure kibana.yml and localhost elastic search port.

### Building for source
-Install dependencies and configuration modify the keys.
```sh
$ pip install -r requirment.txt
$ mv .env.sample .env
$ mv .env.dev.sample .env_dev
```
### Start Elastic Search.
```
$ sudo systemctl start elasticsearch
```
### Start MangoDb Server.
```
$ sudo service mongod start
```
### Start Kibana Server.
```sh
$ sudo -i service kibana start
```
### Run Test cases and Generate Report.
```sh 
$ python -m pytest --cov-report term-missing --cov
$ coverage html -d coverage_html or 
$ coverage html -d coverage_html -i
```

Verify the deployment by navigating to your server address in your preferred browser.
- KIBANA
- ELASTIC SEARCH
- MANGODB

```sh
http://127.0.0.1:5601/app/kibana#/home?_g=()
http://localhost:9200
http://localhost:27017
```
##### Add source data
-Here we need to pass the data from the command line as arguments like wise in below, Name, Code, Url are unique fields 
and required fields

```
 $ python source.py 'Hyderbadevents' 'HYD-EVENTS' 'https://www.hyderabadevents.com/'   
```
Here we need to pass the same way like above.

##### Add Events data:
To run the below command it will crawl and insert events data into database.
```
$ python crawling.py
```
### Todos

 - Needs to implement production environment.
 - Cron Job implementations.
 - Elastic search and test cases.

License
----# eventorg-crawling

