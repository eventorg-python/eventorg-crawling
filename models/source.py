import datetime
from elasticsearch5 import Elasticsearch
from mongoengine import *
from bson.objectid import ObjectId
import settings
import json
from marshmallow import Schema, fields, pprint

es = Elasticsearch()

class Source(Document):

    name = StringField(required=True, unique=True, max_length=200)
    code = StringField(required=True, unique=True)
    url = URLField(required=True, unique=True)
    created_at = DateTimeField(default=datetime.datetime.now)
    updated_at = DateTimeField(default=datetime.datetime.now)

    ## Business Logic Here

    def create_source(source_info):
        status_mess = {}
        try:
            source = Source(name=source_info["name"], code=source_info["code"], url=source_info["url"])
            source.save()
            ## Elastic search functonality needs to add here
            if(source.id):
                ## converting into json object.
                schema = SourceSchema()
                result = schema.dumps(source)
                pprint(result)
                ## Storing data in elastic search
                es.index(index="source-index", doc_type='source-doc', id=source.id, body=result)
                status_mess['status'] = 'successfully created source'

        except Exception as err:
            print('something went while creating source' + str(err))

    def get_source(id):
        try:
            source = Source.objects.get(id=id)
            return source
        except Exception as err:
            print('something went wrong while fetching record ' + str(err))

    def update_source(id, source_params):
        try:
            source = Source.objects.get(id=id)
            source.name = source_params["name"]
            source = source.save()
            ## Elastic search functonality needs to add here
            schema = SourceSchema()
            result = schema.dumps(source)
            es.index(index="source-index", doc_type='source-doc', id=id, body=result)
            return source
        except Exception as err:
            print('some thing went wrong either fetching or updating record' + str(err))

    def delete_source(id):
        status_mess = {}
        try:
            source = Source.objects.get(id=id)
            if source:
                source.delete()
                ## Elastic search functonality needs to add here
                status_mess['status'] = 'successfully deleted source'
                res = es.delete(index="source-index", doc_type='source-doc', id=id)
            else:
                status_mess['status'] = 'No source fould please provide valid source'
            return status_mess
        except Exception as err:
            print('something went wrong while fetching record ' + str(err))


## Here needs to get data from the elastic search
class SourceSchema(Schema):
    name = fields.String()
    code = fields.String()
    url = fields.String()
