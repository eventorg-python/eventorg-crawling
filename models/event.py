import datetime
import json
from mongoengine import *
from marshmallow import Schema, fields, pprint
from elasticsearch5 import Elasticsearch
import settings

es = Elasticsearch()


class Event(Document):
    event_tag_name = StringField(required=True, max_length=200)
    title = StringField(required=True, max_length=200)
    event_info = DictField(required=True)
    contact_info = DictField(required=True)
    source_id = ObjectIdField(required=True)
    created_at = DateTimeField(default=datetime.datetime.now)
    updated_at = DateTimeField(default=datetime.datetime.now)

    def create_events(source, document):
        if document:
            try:
                with open(document) as f:
                    data = json.load(f)
                    for tag_name, value in data.items():
                        for title, event_info in value.items():
                            events = Event.objects(event_tag_name=tag_name, title=title)
                            if(events.count()==0):
                                if event_info:
                                    contact_info = event_info.pop('contact_info')
                                    event_info.pop('title')
                                    event_data = Event(
                                        event_tag_name=tag_name,
                                        title=title,
                                        event_info=event_info,
                                        contact_info=contact_info,
                                        source_id=source['id']
                                    )
                                    event = event_data.save()
                                    if(event.id):
                                        ## Storing data in elastic search
                                        schema = EventSchema()
                                        result = schema.dumps(event)
                                        es.index(index="event-index", doc_type='event-doc', id=event.id, body=result)

                                else:
                                    print('null value')
                                    print(title)
                            else:
                                pass
            except Exception as err:
                return 'something went wrong while creating source' + str(err)
        else:
            return 'Events file is empty'


## Serilizer data
class EventSchema(Schema):
    title = fields.String()
    event_tag_name = fields.String()
    updated_at = fields.DateTime()
    created_at = fields.DateTime()
    source_id = fields.String()
    contact_info = fields.Dict()
    event_info = fields.Dict()
