import sys
from models.source import Source

'''
  Needs to provide sys arguments name, code, url the data is saved in database.
'''


class SourceInfo:

    def __init__(self):
        if len(sys.argv) == 4:
            source_info = dict()
            source_info["name"] = sys.argv[1]
            source_info["code"] = sys.argv[2]
            source_info["url"] = sys.argv[3]
            self.create_source(source_info)
        else:
            raise ValueError('Needs to provide exact 3 command line number arguments as name , code, url')

    def create_source(self, source_info):
        Source.create_source(source_info)


SourceInfo()
