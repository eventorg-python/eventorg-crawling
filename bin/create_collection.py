import sys
import os
import datetime
path = 'db/migration'

name = datetime.datetime.now().strftime("%Y%m%d%H%M%S_"+sys.argv[1])
filename = name+'.py'
with open(os.path.join(path, filename), "w+") as outfile:
    print('Successfully created collection')
