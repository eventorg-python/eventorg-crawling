from models.source import Source
from faker import Faker
from elasticsearch import Elasticsearch
es = Elasticsearch([{'host': 'localhost', 'port': 9200}])

faker = Faker()


def test_create_source(mongodb):
    collection_names = mongodb.collection_names()
    assert 'source' in collection_names
    source = dict()
    source["name"] = faker.name()
    source["url"] = faker.url()
    source["code"] = 'CODE-ONE'
    Source.create_source(source)
    '''
      Done creating source object
    '''
    s_obj = mongodb.source.find_one({'code': 'CODE-ONE'})
    assert 'CODE-ONE' == s_obj['code']
    es.delete(index='source-index', doc_type='source-doc', id=s_obj['_id'])


def test_get_source(mongodb):
    source = mongodb.source.find_one({'code': 'EVENT-CODE-TWO'})
    source_record = Source.get_source(source['_id'])
    assert 'Andersonview' == source_record['name']
    assert 'EVENT-CODE-TWO' == source_record['code']
    assert 'https://miller.com/' == source_record['url']


def test_update_source(mongodb):
    # update source object
    source = mongodb.source.find_one({'code': 'EVENT-CODE-TWO'})
    source_params = {'name': 'updated Andersonview'}
    source_record = Source.update_source(source['_id'], source_params)
    assert 'updated Andersonview' == source_record['name']


def test_delete_source(mongodb):
    # delete source object
    # Before delete source object count

    assert 3 == mongodb.source.count()

    source = mongodb.source.find_one({'code': 'EVENT-CODE-TWO'})
    response = Source.delete_source(source['_id'])

    assert 2 == mongodb.source.count()
    assert 'successfully deleted source' == response['status']

    # after delete source object count
