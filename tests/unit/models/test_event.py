from models.event import Event
from faker import Faker
from models.source import Source

faker = Faker()


def test_create_events(mongodb):
    collection_names = mongodb.collection_names()
    assert 'event' in collection_names
    source = Source.objects().first()
    assert 'EVENT-CODE-ONE' == source['code']
    document = "test_event.json"
    Event.create_events(source, document)
    assert 30 == mongodb.event.count()


def test_create_events_with_error_message(mongodb):
    collection_names = mongodb.collection_names()
    assert 'event' in collection_names
    source = mongodb.source.find_one({'code': 'EVENT-CODE-TWO'})
    assert 'EVENT-CODE-TWO' == source['code']
    document = "test_event.json"
    response = Event.create_events(source, document)
    assert "something went wrong while creating source'id'" == response


def test_create_events_path_not_found_json_file(mongodb):
    collection_names = mongodb.collection_names()
    assert 'event' in collection_names
    source = mongodb.source.find_one({'code': 'EVENT-CODE-TWO'})
    assert 'EVENT-CODE-TWO' == source['code']
    document = "no_file.json"
    response = Event.create_events(source, document)
    assert "something went wrong while creating source[Errno 2] No such file or directory: 'no_file.json'" == response


def test_create_events_while_no_file(mongodb):
    collection_names = mongodb.collection_names()
    assert 'event' in collection_names
    source = mongodb.source.find_one({'code': 'EVENT-CODE-TWO'})
    assert 'EVENT-CODE-TWO' == source['code']
    document = ""
    response = Event.create_events(source, document)
    assert "Events file is empty" == response
