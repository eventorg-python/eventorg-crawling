from search import ElasticSearchExample
from pprint import pprint
import json
from models.source import Source
from pytest_elasticsearch import factories
from elasticsearch import Elasticsearch
elastic = ElasticSearchExample()
es = Elasticsearch([{'host': 'localhost', 'port': 9200}])


## Get Single record
def test_get_and_verify_source(mongodb):
    source_es = es.search(index='source-index', doc_type='source-doc')
    source = source_es['hits']['hits'][0]
    result = elastic.get_source_id(source['_id'])
    assert source['_source']['name'] == source["_source"]["name"]
    assert source['_source']['url'] == source["_source"]["url"]
    assert source['_source']['code'] == source["_source"]["code"]
    assert source['_id'] == source["_id"]

## Get Single record
def test_search_with_by_source_name(mongodb):
    source_es = es.search(index='source-index', doc_type='source-doc')
    source = source_es['hits']['hits'][0]
    body = dict()
    body["query"] = dict()
    body["query"]["match"] = dict()
    body["query"]["match"]["name"] = "google-hyderabad-events"
    body = json.dumps(body)
    result = elastic.search_source(body)
    assert 10 == int(result['hits']['total'])

## Exact phase
def test_search_with_by_source_code(mongodb):
    source_es = es.search(index='source-index', doc_type='source-doc')
    source = source_es['hits']['hits'][0]
    body = dict()
    body["query"] = dict()
    body["query"]["match_phrase"] = dict()
    body["query"]["match_phrase"]["url"] = "https://www.senecaglobalhyd.com/"
    body = json.dumps(body)
    result = elastic.search_source(body)
    assert 1 == int(result['hits']['total'])

### pagination
def test_search_with_pagination(mongodb):
    body = dict()
    body["from"] = 1
    body["size"] = 5
    body["query"] = dict()
    body["query"]["match_all"] = dict()
    body = json.dumps(body)
    result = elastic.search_source(body)
    assert 5 == result["_shards"]["total"]

## sorting asc and desc
def test_search_with_sort_asc(mongodb):
    body = dict()
    body["sort"] = list()
    body["sort"].append({"code": "desc"})
    body["query"] = {"match_all": {}}
    body = json.dumps(body)
    result = elastic.search_source(body)
    assert "google-hyderabad-events" == result['hits']["hits"][0]["_source"]["name"]

def test_search_with_sort_desc(mongodb):
    body = dict()
    body["sort"] = list()
    body["sort"].append({"name": "asc"})
    body["query"] = {"match_all": {}}
    body = json.dumps(body)
    result = elastic.search_source(body)
    assert "a-events" == result['hits']["hits"][0]["_source"]["name"]
