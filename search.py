# from elasticsearch import Elasticsearch
# #
# # es = Elasticsearch([{'host':'localhost','port':9200}])
# # print(es)
# #
# # e1={
# #     "first_name":"nitin",
# #     "last_name":"panwar",
# #     "age": 27,
# #     "about": "Love to play cricket",
# #     "interests": ['sports','music'],
# # }
# #
# # res = es.search(index='source-index', body={'query': {'match_all': {}}})
# # print('Got %d hits:' %res['hits']['total'])
# # print(res['hits']['hits'])
#
# from marshmallow import Schema, fields, pprint
# from models.event import EventSchema, Event
# from mongoengine import *
#
#
# event = Event.objects.first()
#
# schema = EventSchema()
# result = schema.dumps(event)
# es = Elasticsearch()
#
# es.index(index="event-index", doc_type='event-doc', id=event.id, body=result)
#
# pprint(result)


from elasticsearch import Elasticsearch
es = Elasticsearch([{'host': 'localhost', 'port': 9200}])


class ElasticSearchExample:

    def __init__(self):
        print('Initial method')

    def get_source_id(self, source_id):
        result = es.get(index='source-index', doc_type='source-doc', id=source_id, _source=["url", "name", "code"])
        return result

    def search_source(self, body):
        result = es.search(index='source-index', doc_type='source-doc', body=body,
                           _source_include=["url", "name", "code"])
        return result

